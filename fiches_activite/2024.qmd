---
title: "Fiche d'activité 2024"
subtitle: "Bilan 2024"
author:
  - name: Olivier Rué
    orcid: "0000-0001-7517-4724"
    affiliations: "MaIAGE - Migale"
date: "2025-02-19"
lang: fr
image: preview.png
date-modified: today
categories: [INRAE]
fig-cap-location: bottom
tbl-cap-location: top
number-sections: false
format:
  html:
    dot-format: png
    toc-location: left
    page-layout: full
    code-annotations: select
editor: 
  markdown: 
    wrap: sentence
execute:
   engine: knitr
freeze: true
lightbox: true
---

# Identité

- Agent : Olivier Rué
- Responsable de l'entretien : Valentin Loux

# Suivi parcours et carrière

## Emplis et fonctions antérieurs à INRAE

RAS

## Carrière depuis l'entrée à INRAE

RAS

## Activités hors INRAE (mandat, cumul d'emploi, ...)

RAS

## Diplômes

RAS

# Activités et missions

## Intitulé du poste

`Ingénieur bioinformaticien sur la plateforme Migale, responsable du service d'analyse de données`

## Activités principales

**Conduire des projets d’analyse de données omiques**

- Je participe au montage et à la rédaction d'appels à projets (ANR, métaprogrammes...).
- Je choisis les approches appropriées à la problématique biologique.
- Je lance les analyses grâce aux ressources mises à disposition par la plateforme.
- Je développe de nouveaux outils, programmes ou workflows d'analyse si nécessaire.
- Je restitue les résultats aux demandeurs.
- Je participe à leur valorisation.

**Accompagner les biologistes qui le souhaitent à acquérir des compétences en bioinformatique**

- Je les conseille d’un point de vue technique et scientifique dans leurs premières analyses.
- Je documente et rend public dès que possible mes analyses, mes tests et mes retours d'expérience, sous forme de notes, de tutoriels ou de présentations.
- Je mets en place et donne des formations dans le cadre du cycle « Bioinformatique par la pratique » et dans d’autres cadres à l’échelle nationale.

**Responsable du processus « Analyse de données »**

- Je mets en place un cadre rigoureux et documenté pour cette activité qui concerne 6 autres personnes de la plateforme.
- Je définis les objectifs annuels du processus en collaboration avec mes responsables.
- Je suis audité une à deux fois par an dans le cadre des audits internes/externes pour la norme LRQA ISO9001-2015 .

**Gestion des sites web de la plateforme**

- Je développe, fais évoluer et alimente les sites web de la plateforme.

**Contribution au fonctionnement quotidien de la plateforme de service**

- Je participe à l’administration de l’instance Galaxy de la plateforme.
- Je participe au support utilisateurs quotidien (+800 utilisateurs actifs).

### Missions complémentaires

**Agent de prévention (lettre de mission, 10%)**

- J'assiste et conseille la direction de l'unité dans la démarche d’évaluation des risques, la mise en place d’une politique de prévention, la mise en œuvre des consignes de sécurité et d’hygiène au travail.

## Position dans l'organisation (liens hiérarchiques et fonctionnels) et implication dans les projets collectifs

**Collaborations dans l'unité :**

- Je travaille avec V. Loux, le responsable opérationnel de la plateforme Migale pour déterminer la feuille de route du service d’analyse et pour les autres missions liées à la plateforme.
- Je travaille en étroite collaboration avec C. Midoux, V. Loux, C. Hennequet-Antier et M. Mariadassou dans le cadre des projets d’analyse de données dans lesquels nous sommes impliqués.
- Je travaille régulièrement avec les autres membres de la plateforme MIGALE pour participer au support utilisateurs, à la gestion des sites web et à l’amélioration des ressources mises à disposition par la plateforme.
- Je travaille en collaboration avec plusieurs membres de l’unité pour la mise en place et la dispense de formations, dans le cadre du cycle Bioinformatique par la pratique (V. Martin, V. Loux, C. Midoux, M. Mariadassou).
- J'accueille les nouveaux arrivants dans mon bâtiment pour leur faire un point Prévention.

**Collaborations hors unité :**

- Je collabore avec les personnes qui ont demandé une collaboration ou un accompagnement pour l’analyse bioinformatique de leurs données de séquençage. Il s’agit de chercheurs, ingénieurs ou doctorants d’unités ou d’UMR INRAE, académiques hors INRAE ou de sociétés privées. Dans le cadre de projets d’accompagnement, certains demandeurs sont accueillis temporairement sur la plateforme pour faciliter le travail collaboratif.
- Je travaille avec G. Pascal, V. Darbot, G. Agoutin (GenPhyse, Toulouse), M. Bernard (GABI, Jouy-en-Josas) et L. Auer (UMR IAM, Nancy) sur la mise au point et le développement de FROGS, un outil d’analyse de données de métagénomique ciblée (metabarcoding) à destination des biologistes. Nous échangeons plusieurs fois par semaine (support utilisateurs, développement…).
- Je travaille avec des membres du CATI BOOM (Bioinformatics for Omics and metaOmics of Microbes) sur différentes tâches et projets décidés conjointement (cette année, recensement et tests de packages R dédiés à la visualisation de données de microbiome avec C. Heenequet-Antier, J. Aubert, C. Midoux, F. Kempf et E. Drula). Nous nous réunissons deux à trois fois par an pour faire avancer les réalisations et échanger sur des thématiques d’intérêt pour le CATI.
- J’échange avec les membres du groupe de travail « Métagénomique, identification d'espèces et phylogénie » du PEPI (Partage d’Expérience et de Pratiques en Informatique) Ingénierie Bioinformatique et Statistique pour les données haut-débit (IBIS). Nous nous réunissons deux à trois fois par an pour échanger et présenter nos travaux.
- J’échange avec les bioinformaticiens du centre de Jouy-en-Josas dans le cadre de réunions spécifiques qui ont lieu tous les mois.
- J’interagis avec des collègues bioinformaticiens (INRAE et hors INRAE) pour la mise en place et la dispense de formations (AVIESAN/IFB/Université).
- J'échange avec mes collègues de la Commision d'Évaluation des Ingénieurs (CEI) qui a lieu chaque année pendant 2 à 3 jours.

**Projets financés :**

Voici les projets financés dans lesquels j'interviens (encadrement ou analyse) mais pour lesquels je n'ai pas participé à la rédaction :

- MUDIS4LS-IS5 (PIA IFB - 2021-2029)
  - Mutualised Digital Spaces for FAIR data in Life and Health Science
- DEEP IMPACT (ANR - 2021-2027)
  - Analyse des interactions plante-microbiote pour promouvoir la défense des plantes aux bioagresseurs
- MICROFLU4AMR (PPR ANR - 2023-2027)
  - Characterization and high-throughput screening of bacterial communities in the soil: mechanisms of antibiotic resistance and discovery of new antibiotics. J'encadrerai un CDD pendant 2 ans à partir de 2025, il sera basé à Dijon dans l'équipe de recherche des partenaires.
- MetaPDOCheese (France Génomique - 2017-2021)
  - analyse de la diversité microbienne et fongique des fromages AOP français)
- FONCTIOMIC (métaprogramme MEM - 2018-2021)
  - Lien entre la dynamique des métagénomes microbiens et la décomposition de la matière organique dans les sols agricoles
- MICROVARIOR (métaprogramme METABIO - 2020-2022)
  - Vins bio de variétés résistantes : analyse du microbiote des baies et de sa capacité à conduire des fermentations spontanées en bio et zéro phyto

Voici les projets financés pour lesquels j'ai été impliqué dans la rédaction : 

- RESILIENS (CARNOT F2E - 2024-2026)
  - Impact de l’interaction entre le microbiome respiratoire et le glycome sur la sensibilité des bovins aux infections. J'encadrerai un Post-Doc de 2025 à 2026 sur les aspects bioinformatiques.
- REGLYS (métaprogramme SANBA - 2024-2027)
  - Rôle de l’axe glycome-microbiome sur la susceptibilité aux infections virales bovines et le bien-être des animaux
- HYPHAMAIZE (ANR - 2025-2027)
  - Determining the role of HYPHospheric bacteria in the Arbuscular mycorrhizal fungi-mediated nutrient supply and growth promotion of MAIZE under low-input agriculture

## Conditions particulières d'exercice

RAS

## Principaux interlocuteurs internes et externes

![](orgfunc2024.png)

**Activités de plateforme**

J'ai beaucoup d'interactions au sein de l'équipe Migale pour toutes les activités liées à la plateforme (support, administration Galaxy, développement et maintenance des sites web, formations) et grâce au processus Analyse de données dont je suis le responsable. Les interlocuteurs internes sont donc les membres de la plateforme et les interlocuteurs externes sont les utilisateurs des services de la plateforme.

Je forme de nombreux scientifiques dans le cadre du cycle de formation Migale ou de formations organisées par d'autres instituts où je suis sollicité régulièrement.

**Projets d'analyse**

Les demandeurs d'analyse sont des chercheurs, des ingénieurs ou des doctorants provenant de différents instituts publics comme du privé.

**Projets de développement**

Je fais partie du groupe FROGS pour lequel je collabore depuis 2015 avec des ingénieurs INRAE basés sur différents sites (Jouy-en-Josas, Toulouse, Nancy). Nous échangeons par visio-conférence et par la messagerie instantanée Mattermost proposée par la forgeMIA.

**Réseau**

Je participe régulièrement aux manifestations organisées par le CATI BOOM, le PEPI IBIS, l'infrastructure de recherche BioInfOmics, l'institut Français de Bioinformatique...
Pour la veille scientifique de mon domaine, je me rends aux séminaires de l'équipe de recherche StatInfOmics de l'unité.

**Assistant de prévention**

Je suis en relation avec le réseau des Assistants de Prévention du centre de Jouy-en-Josas.

## Missions transverses

- Conseil scientifique de l'unité MaIAGE (depuis 2024).
- Assistant de prévention (depuis 2024).
- Participation au comité utilisateurs de la forge MIA (depuis 2022).
- Évaluateur CEI (depuis 2021).
- Sauveteur Secouriste du Travail (depuis 2017).

# Bilan de l'année écoulée

**Bilan relatif aux objectifs formulés l'année précédente**

Les chiffres que je présente sont en date du 5 février 2025.

**Mission principale : Analyse de données omiques et gestion du service d'analyse**

En 2024, 34 projets ont été menés, avec 22 nouveaux projets initiés et 22 finalisés. Le détail des projets est accessible ici : https://documents.migale.inrae.fr/project_metrics.html. Parmi eux, 57% ont été sollicités par des scientifiques du département MICA, soulignant l'importance du service d'analyse pour les scientifiques de mon département de rattachement.

Le fonctionnement interne du service est fluide, avec une bonne intégration des procédures par les six membres du service. J'ai mis en place des processus facilitant la gestion des projets, notamment en allégeant les tâches administratives pour mes collègues et en automatisant des actions telles que la création de fiches de suivi, la remontée d'indicateurs et le déploiement des rapports d'analyse.

Les retours des demandeurs attestent de la qualité du service rendu, tant en termes de respect des délais et des livrables que des interactions avec l'équipe. Les commentaires sont consultables ici : https://documents.migale.inrae.fr/data-analysis.html.

Les biologistes apprécient particulièrement les rapports web disponibles, comme l'indiquent les enquêtes de satisfaction. Le site web que je maintiens comprend désormais 9 tutoriels (+2), 10 actualités (+5), 10 présentations (+4) et 36 rapports d'analyse (+22), adoptés par l'ensemble de la plateforme.

Dans mon rôle de responsable de processus, j'ai automatisé le calcul des métriques clés, optimisant ainsi le suivi et la transparence des projets. Ces métriques, publiques, participent à l'attractivité du service. Cet effort a été particulièrement mis en avant lors du dernier audit interne.

Je suis co-auteur de 3 publications scientifiques sur l'année 2024.



- A comprehensive, large-scale analysis of “terroir” cheese and milk microbiota reveals profiles strongly shaped by both geographical and human factors ; Françoise Irlinger, Mahendra Mariadassou, Eric Dugat-Bony, Olivier Rué, Cécile Neuvéglise, Pierre Renault, Etienne Rifa, Sébastien Theil, Valentin Loux, Corinne Cruaud, Frederick Gavory, Valérie Barbe, Ronan Lasbleiz, Frédéric Gaucheron, Céline Spelle, and Céline Delbès ; ISME Communications

- In-depth characterization of a selection of gut commensal bacteria reveals their functional capacities to metabolize dietary carbohydrates with prebiotic potential ; Cassandre Bedu-Ferrari, Paul Biscarrat, Frederic Pepke, Sarah Vati, Cyril Chaudemanche, Florence Castelli, Céline Chollet, Olivier Rué, Christelle Hennequet-Antier, Philippe Langella, and Claire Cherbuy ; mSystems

- Easy16S: a user-friendly Shiny web-service for exploration and visualization of microbiome data. ; Cédric Midoux, Olivier Rué, Olivier Chapleur, Ariane Bize, Valentin Loux, and Mahendra Mariadassou ; Journal of Open Source Software



**Engagement dans la recherche et encadrement**

Depuis 2023, j'ai exprimé le souhait de m'investir davantage dans des projets de recherche d'envergure et d'encadrer du personnel contractuel.

- Participation à la rédaction de plusieurs appels d'offres (ANR, métaprogrammes...). Les détails se trouvent dans la partie Bilan.
- Recrutement (fin 2024) et co-encadrement de Marie Lahaye pour le projet MUDIS4LS-IS5 (2025-2027), basée à Rennes.
- Mise en place de solutions pour un encadrement optimal (traçabilité, espaces de travail collaboratifs, réunions en visioconférence, visites régulières).

**Activités de formation et diffusion de l'expertise**

- Formation "Métagénomique shotgun" (2 jours, 6 formés) et "Initiation à Linux" (1 jour, 5 formés).
- Présentations orales lors de divers évènements (PEPI, Migale&Vous, séminaire interne).
  - https://hal.inrae.fr/hal-04963446
  - https://hal.inrae.fr/hal-04786277v1
- Présentation pour la visite HCERES (seule présentation scientifique de l'équipe)
  - https://hal.inrae.fr/hal-04963473
- Sollicitation par Cécile Lecoeur (CNRS) pour une présentation sur la métagénomique (groupe de travail Statistique & Génomique du RIS), que je n'ai pu honorer en raison d'un conflit d'agenda.
- Participation au "MARCO-BOLO Data Analysis Challenge", projet international, en tant qu'expert en métabarcoding via FROGS.

Voici maintenant le retour sur les objectifs qui étaient les miens pour l'année 2024.

_Objectif 1 : leadership sur un projet/thématique_

_Le premier objectif est de devenir leader sur un projet ou une thématique. Pour cela, je serai pro-actif dans le montage de projets, soit en m'intégrant à des projets qui sont en train d'émerger (Mudis4LS, AAP Ferments du Futur...) et/ou en participant à leur montage. L'objectif au sein de l'équipe est également de se positionner sur des gros projets, donc des opportunités devraient se présenter._

Bilan :
J'ai participé au montage de plusieurs projets financés en 2024, en étant pour la plupart workpackage leader :

- HOLOFLUX - SporeButyrate (Supplémentation orale de spores de bactéries commensales butyrogènes pour restaurer la production de butyrate au sein d’un microbiote colique dysbiotique) porté par Sandrine Auger (MICALIS, dept MICA) - Refusé 
- HOLOFLUX - VolatHoloMiC (Relation Holobionte - Clostridioides difficile: dépistage par des biomarqueurs volatils et identification de mesures réalistes de contrôle du risque sanitaire) porté par Isabelle Poquet (MICALIS, dept MICA) - Réfusé 
- projet Carnot F2E - RESILIENS (Impact of the respiratory microbiome-glycome interplay on the bovine susceptibility of cattle to infections) porté par Nuria Mach (dept SA) - Accepté 
- ANR - AAPG 2025: HyphAMaize (Determining the role of HYPHospheric bacteria in the Arbuscular mycorrhizal fungi-mediated nutrient supply and growth promotion of MAIZE under low-input agriculture) porté par Benoît Alunni (dept BAP) - En attente
- JGI Community Science Program (CSP) - Exploring historical soil metagenomes - impact of current environmental changes on soil microbial diversity porté par Roland Marmeisse (MHN) - Refusé

J'ai intégré le projet Mudis4LS (IS5) via le recrutement et l'encadrement avec Fabrice Legeai (IGEP, Rennes) de Marie. Son objectif est de développer un système d’information facilitant la collecte, la validation puis l’intégration de l’ensemble des données et métadonnées issus de projets travaillant autour de l'holobionte. Il faudra identifier les partenaires intéressés, évaluer leurs besoins, les objets d'études, et à partir de cela, proposer des bonnes pratiques et des solutions pour gérer au mieux ces jeux de données, les enrichir de connaissances existantes et les rendre accessibles à la communauté. C'est un projet qui se veut structurant pour la communauté travaillant sur l'holobionte.

_Objectif 2 : finaliser la version 5.0 de FROGS_

_Le second objectif est de finaliser la version 5.0 de FROGS, en proposant un outil avec les nouvelles fonctionnalités disponibles à la communauté sur un dépôt conda._

Bilan :

Cet objectif est rempli. J'ai développé denoising.py, un outil permettant à la fois d'utiliser swarm ou DADA2 pour générer des ASVs à partir de fichiers FASTQ. L'outil a été testé sur divers jeux de données et validé. J'en ai profité pour régler différents bugs et implémenté quelques améliorations :

- Intégration des graphiques issus de cluster_stats.py et affiliation_stats.py aux rapports HTML des outils concernés.
- Implémentation de thèmes personnalisables en JavaScript.
- Dépôt Conda validé le 7 juin 2024.
- Développement en python, R et javascript.

Le projet FROGS comprend désormais, avec le chiffre indiquant ma contribution en nombre de lignes depuis la dernière release :

- 91 fichiers Python (15 867 lignes, +1 000)
- 13 fichiers HTML (16 569 lignes, +2 000)
- 14 fichiers R/Rmd (841 lignes, +400)

J'ai développé seul, n'ayant pas pu bénéficier de l'aide du CDD sur le projet (autres priorités) et de la non disponibilité de M. Bernard, occupée par sa thèse. Elle m'a cependant accompagné pour la création du package Conda.

_Objectif 3 : site web banques FROGS_

_Le troisième objectif sera de développer avec l'aide de ma stagiaire de M2 un site web pour les banques de données mises à disposition dans l'outil FROGS. L'objectif est de fournir aux utilisateurs de FROGS des ressources/informations en ligne leur permettant de choisir les banques adéquates pour leur sujet._

Bilan :

Cet objectif est également rempli. Le site est en ligne: https://databanks-frogs-4e6db8ad311737fbffa50110cb95e53a89646eef0a3337c.pages.mia.inra.fr/. 

Fonctionnalités :

- Informations sur 78 banques (nombre de séquences, distribution des longueurs, composition taxonomique).
- Recherche de taxons et visualisation via Krona chart.
- Code disponible sur la forge du département MathNum (forgeMIA).

Je vais poursuivre la publicité (présentation effectuée lors d'une réunion du PEPI IBIS fin 2024), demander une URL pérenne (`frogs-databanks.inrae.fr`) et documenter en quoi ce service web permet de répondre à des questions d'intérêt pour les utilisateurs de FROGS et au-delà.

# Fonctions d'encadrement

## Bilan de l'activité managériale et éléments de contexte (bilan effectué par l'agent)

L. Bekaï, étudiante en M2 bioinformatique à l’université d’Aix-Marseille, m’avait contacté une première fois l’année précédente pour un stage que j’avais proposé. Toutefois, en concertation avec G. Pascal, nous avons finalement décidé de ne pas donner suite en raison d’un changement de priorités sur FROGS.
Elle m’a recontacté fin 2023, et après un entretien au cours duquel j’ai apprécié sa motivation et son intérêt pour le métier de bioinformaticien sur une plateforme, nous avons organisé son accueil. Nous avons notamment monté ensemble un dossier pour lui permettre de loger dans une chambre louée par le centre pendant son stage.
Afin de faciliter son intégration et sa prise en main du sujet, j’ai adapté mes habitudes de travail et l’ai accompagnée de près durant les deux premiers mois, en échangeant avec elle presque chaque matin. J’étais ravi de m’investir dans son encadrement, comme l’avaient fait mes propres tuteurs lors de mes études et au début de ma carrière.

Avec du recul, mon bilan est contrasté. 
Sur le plan technique, le projet a avancé : un site web référençant les banques de données disponibles dans FROGS a été déployé. Cependant, j’ai dû réaliser environ 90 % des tâches qui lui avaient été confiées pour que le projet avance.

Son investissement a été insuffisant et son attitude peu professionnelle : horaires de travail irréguliers, manque de rigueur dans la vérification du code, retards aux réunions, et faible implication. Malgré de nombreuses discussions pour lui expliquer mes attentes et la nécessité d’un engagement plus sérieux, aucun changement notable n’a eu lieu. La direction de l'unité m'a soutenu et a rappelé les règles de bonne conduite au travail. J’ai également signalé cette situation à son référent de master, qui n’a pas semblé s’en inquiéter et a même minimisé ces aspects lors du débriefing du stage avec le jury.

Cette expérience me conforte dans l’idée qu’à l’avenir, je n’hésiterai pas à demander des références avant d’accueillir un stagiaire.

# Objectifs de l'année passée

## Objectifs annuels individuels

Je me fixe trois objectifs principaux pour l'année 2024. 

- Le premier est de devenir leader sur une projet ou une thématique. Pour cela, je serai pro-actif dans le montage de projets, soit en m'intégrant à des projets qui sont en train d'émerger (Mudis4LS, AAP Ferments de Futur...) et/ou en participant à leur montage. L'objectif au sein de l'équipe est également de se positionner sur des gros projets, donc des opportunités devraient se présenter.
- Le second objectif est de finaliser la verion 5.0 de FROGS, en proposant un outil avec les nouvelles fonctionnalités présentées précédemment disponible à la communauté sur un dépôt Conda. 
- Le troisième sera de développer avec l'aide de ma stagiaire de M2 un site web pour les banques de données mises à disposition dans l'outil FROGS. L'objectif est de fournir aux utilisateurs de FROGS des ressources/informations en ligne leur permettant de choisir les banques adéquates pour leur projet. 

# Objectifs pour l'année à venir

## Objectifs annuels individuels

**Piloter le projet de développement d'une interface Shiny permettant de propager l'expertise taxonomique des ASVs obtenus dans des projets passés, avec C. Midoux et M. Mariadassou**

L'objectif est de fournir aux scientifiques une interface permettant de bénéficier des expertises d'ASVs déjà effectuées pour les propager sur de nouveux jeux de données. L'utilisateur pourrait charger son fichier BIOM de sortie de FROGS (ou autre logiciel) et confronter ses ASVs à ceux déjà expertisés et choisir de remplacer l'affiliation (souvent Multi-affiliation ou mal affilié) par celle déjà expertisée, dans un de ses précédents projets ou par un autre collaborateur.
M. Mariadassou et C. Midoux ont accepté de participer à ce projet de développement que je piloterai.

**Organiser la réflexion et les échanges sur le partage de codes/pratiques au sein de MaIAGE (un objectif issu des réflexions menées lors de la préparation de visite HCERES pour mutualiser les pratiques au sein de l'unité)**

La première étape sera de rédiger du contenu sur Quarto (avec C. Midoux) et de discuter dans un second temps avec M. Mariadassou et V. Loux, les responsables des équipes Migale et StatInfOmics de la meilleure façon de faire pour initier un partage de pratiques entre les membres des deux équipes intéressés (journées scientifiques de l'unité, séminaire, demie-journée d'échanges...)

**Créer des liens avec des projets et des groupes de travail existants (DEEP IMPACT, IFB, Ferments du Futur...)**

À travers l'encadrement de M. Lahaye, des opportunités vont se présenter pour intégrer des groupes de travail ou des collectifs travaillant sur les ontologies et sur l'holobionte. J'accompagnerai Marie à ces réunions et évènements pour que son travail puisse bénéficier du travail déjà effectué et qu'elle participe aux réflexions pour proposer des solutions.

**Organiser l'évènement FROGSDays en fin d'année 2025**

Nous avons décidé d'organiser un évènement sur 2 ou 3 jours, à Toulouse, en fin d'année 2025 pour fêter les 10 ans de FROGS. Nous souhaitons présenter les avancées, proposer des présentations scientifiques sur des projets passés, inviter des scientifiques et/ou ingénieurs travaillant sur le futur du métabarcoding...

**Trouver une alternative à Highcharts**

Je vais recruter et encadrer avec M. Bernard un(e) stagiaire de niveau Master 1 pour tester des bibliothèques Javascript proposont des fonctionnalités similaires à Highcharts, la librairie utilisée dans FROGS mais qui a changé ses conditions d'utilisation. Nous devons changer pour respecter la nouvelle licence associée.

| Priorisation | Objectif | Réalisation, moyens et niveau d'attente |
|---------|:-----|------:|
| 1      | Piloter le projet de développement d'une interface Shiny permettant de propager l'expertise taxonomique des ASVs obtenus dans des projets passés, avec C. Midoux et M. Mariadassou   | Piloter le projet |
| 1      | Organiser la réflexion et les échanges sur le partage de codes/pratiques au sein de MaIAGE (un objectif issu des réflexions menées lors de la préparation de visite HCERES pour mutualiser les pratiques au sein de l'unité) | Préparer des supports documentés et mener les réflexions avec les personnes intéressées dans l'unité |
| 1      | Créer des liens avec des projets et des groupes de travail existants (DEEP IMPACT, IFB, Ferments du Futur...) | Aller chercher des partenaires intéressés, participer aux réunions des différents réseaux concernés, et organiser la réflexion |
| 1      | Organiser l'évènement FROGSDays en fin d'année 2025   | Organiser, partiper au comité de programme, faire la publicité et faire jouer mon réseau pour que les gens y participent, et préparer une présentation |
| 2      | Participer au développement des wrappers de la version 5 de FROGS   | Solliciter des experts Galaxy (B. Batut identifiée) pour choisir la bonne solution technique et seconder M. Bernard |
| 3      | Trouver une alternative à Highcharts   | Encadrer un(e) stagiaire pour tester différentes solutions et les implémenter ensuite |


# Perspectives d'évolution

## Souhaits d'évolution sur le poste actuel

Je souhaite poursuivre ma prise de responsabilités organisationnelles sur des projets d'analyse importants et financés et encadrer des personnels non permanents. Je prévois de me préparer aux concours afin d'évoluer vers un poste d'ingénieur de recherche.

## Souhaits de mobilité et/ou perspectives d'évolution professionnelle interne/externe à INRAE

# Télétravail

## Bilan de l'agent

Aucun souci côté télétravail.
Entre les formations que je donne, les réunions où la présence est nécessaire, les audits, les jours non télétravaillables côté équipe et unité, je n'ai pas établi de jours fixes.

# Formation

## Historique des formations suivies (5 dernières années)

| Intitulé de la formation | Organisme | Année |
|---------|:-----|------:|
| GIT Niveau intermédiaire/avancé    |    |    2022 |
| Présidents et membres de jurys de concours internes    |    |    2023 |
| E Learning Violence Sexuelle et Sexiste    |    |    2024 |
| PREVENTEO | | 2024 |
| Assistants de Prévention initiale | | 2024 |
| Qualité en Recherche (MOOC) | FUN | 2024 |

## Bilan de l'agent: effectivité et utilité des formations année N-1

C. Guérin, l'assistant de prévention de mon bâtiment, m'a sollicité pour le rejoindre dans cette mission, afin de mieux répartir les tâches et éviter qu'il les assume seul.
J'ai donc suivi la formation initiale d'assistant de prévention sur une semaine, une expérience enrichissante qui m'a convaincu de l'importance de ce rôle. Suite à cela, une lettre de mission et une feuille de route m'ont été attribuées par l'unité, m'autorisant à y consacrer jusqu'à 10 % de mon temps.
Notre unité étant moins exposée aux risques que les structures voisines, ma mission principale consiste à assurer l'accueil des nouveaux arrivants. Lors de mes premières interventions, j'y ai consacré environ 30 minutes.
Grâce à une bonne organisation, ces responsabilités n'impactent pas mes activités principales. Cette formation représente bien plus qu'une simple formalité : elle renforce mon engagement envers l'unité.

L'institut a mis en avant la formation en ligne "Qualité en Recherche", une opportunité que j’ai souhaité saisir afin d’approfondir mes connaissances, déjà ancrées dans mon rôle de responsable de processus sur la plateforme. Cette formation m'a renforcé dans la conviction que l’amélioration continue est essentielle à nos activités, en tant qu'agent de plateforme mais plus largement aussi. Adopter cette démarche collective permet d'offrir un service de la plus haute qualité.

## Expression des souhaits de formation pour les 3 années à venir

| Priorisation | Thématiques de formation et compétences visées | Mobilisation du CPF | Échéance prévisionnelle (année) |
|---------|:-----|------:|------:|
| 1    | encadrant de proximité   |     | 2026 | 

# Acronymes utilisés

- PEPI : Partage d'expérience et de pratiques en informatique
- IBIS : Ingénierie Bio Informatique et Statistique
- CATI : Centre automatisé de Traitement de l'Information
- BOOM : Bioinformatics for Omics and metaOmics of Microbes
- ANR : Agence Nationale de la Recherche
- PPR : Programme Prioritaire de Recherche
- SST : Sauveteur Secouriste du Travail
- LRQA : Lloyd's Register Quality Assurance
- MOOC : Massive Open Online Course
- HTML : HyperText Markup Language
- CDD : Contrat à Durée Déterminée
- RMD : R Markdown
- CNRS : Centre National de la Recherche Scientifique
- HCERES : Haut Conseil de l'Évaluation de la Recherche et de l'Enseignement Supérieur
- IS : Implementation Study
- PIA : Programme d'Investissements d'Avenir
- INRAE : Institut National de Recherche pour l'Agriculture, l'alimentation et l'Environnement
- MHN : Muséeum d'Histoire Naturelle
- BAP : Biologie et Amélioration des Plantes
- MICA : MIcrobiologie et Chaîne Alimentaire
- FUN : France Université Numérique
- F2E : France Futur Élevage
- SANBA : SAnté et Bien-être des Animaux en élevage